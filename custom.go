package ndarchive

// CustomValues is for if a bot has custom information it wants to attach, it can do so via this
// while not expliticly required it is recommended to namespace the keys to reduce risk of conflict
// if multiple extensions are used, the namespace name can be anything but try to make it unique
// to namespace use :: as a separator
type CustomValues map[string]string
