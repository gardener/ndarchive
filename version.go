package ndarchive

// Version consts
const (
	// the current major version
	VersionMajor = 1
	// the current minor version
	VersionMinor = 0
)

// Version is a struct representation of a semiver value
// no special parsing logic its just formatted into a literal json representation for simplicity
type Version struct {
	Major uint `json:"major"` // This is incremented for changes that are backwards incompatible, and may break tools like viewers
	Minor uint `json:"minor"` // This is incremented for changes that are backwards compatible, such as a new field or function being included
}
