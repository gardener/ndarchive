package ndarchive

type User struct {
	ID            Snowflake `json:"id"`
	Username      string    `json:"username"`
	Avatar        string    `json:"avatar"`
	Discriminator string    `json:"discriminator"`
	Bot           bool      `json:"bot"`

	Nickname string   `json:"nickname"`
	Roles    []string `json:"roles"`

	// are these available or useful?
	//Joined      Timestamp `json:"joined_at"`
	//Banner      string    `json:"banner,omitempty"`
	//AccentColor int       `json:"accent_color,omitempty"`
	//PremiumType int       `json:"premium_type,omitempty"`
	//Flags       int       `json:"flags,omitempty"`
	//PublicFlags int       `json:"public_flags,omitempty"`

	// custom fields for the archive format
	Webhook bool `json:"webhook"` // is this user an instance of a webhook, see the comment in FileUsers for details on handling webhooks
	Color   int  `json:"color"`   // what color is this user

	// if a bot has custom information it wants to attatch, it can do so here
	Custom CustomValues `json:"custom,omitempty"`
}

type Channel struct {
	// ID       Snowflake `json:"id"` // is this needed? its included in a map with the ID as a key
	Name  string `json:"name"`
	Topic string `json:"topic,omitempty"`
	// Type     int    `json:"type"`                // probably useless but maybe not
	NSFW     bool   `json:"nsfw"`                // so viewers can display a warning
	ParentID string `json:"parent_id,omitempty"` // only needed if multiple channels are included
	Position int    `json:"position,omitempty"`

	// if a bot has custom information it wants to attatch, it can do so here
	Custom CustomValues `json:"custom,omitempty"`
}

type Emoji struct {
	ID       Snowflake `json:"id"`
	Name     string    `json:"name"`
	Animated bool      `json:"animated"`
	// roles
	// user
	// require colons
	// managed
	// Available

	// if a bot has custom information it wants to attatch, it can do so here
	Custom CustomValues `json:"custom,omitempty"`
}

type Role struct {
	// ID   Snowflake `json:"id"` // is this needed? its included in a map with the ID as a key
	Name string `json:"name"`

	Position int `json:"position"`
	Color    int `json:"color"`

	// probably not necessary?
	//Permissions string `json:"permissions,string"`
	//Hoist       bool   `json:"hoist"`
	//Managed     bool   `json:"managed"`
	// Mentionable bool   `json:"mentionable"`

	// if a bot has custom information it wants to attatch, it can do so here
	Custom CustomValues `json:"custom,omitempty"`
}

type Guild struct {
	ID   Snowflake `json:"id"`
	Name string    `json:"name"`
	Icon string    `json:"icon"`

	// --- would these have use for inclusion?
	//Splash          string `json:"splash,omitempty"`
	//DiscoverySplash string `json:"discovery_splash,omitempty"`

	//OwnerID string `json:"owner_id"` // unnecessary?

	//VanityURLCode string `json:"vanity_url_code,omitempty"` // hmm
	//Description   string `json:"description,omitempty"`
	//Banner        string `json:"banner,omitempty"`

	//ApproximateMembers uint64 `json:"approximate_member_count,omitempty"`

	// if a bot has custom information it wants to attatch, it can do so here
	Custom CustomValues `json:"custom,omitempty"`

	// --- unnecessary
	/*
		Owner                  bool    `json:"owner,omitempty"`
		Widget                 bool    `json:"widget_enabled,omitempty"`
		SystemChannelFlags     int     `json:"system_channel_flags"`
		Verification           int     `json:"verification_level"`
		Notification           int     `json:"default_message_notifications"`
		ExplicitFilter         int     `json:"explicit_content_filter"`
		NitroBoost             int     `json:"premium_tier"`
		MFA                    int     `json:"mfa"`
		ApproximatePresences   uint64  `json:"approximate_presence_count,omitempty"`
		MaxVideoChannelUsers   uint64  `json:"max_video_channel_users,omitempty"`
		PublicUpdatesChannelID string  `json:"public_updates_channel_id"`
		PreferredLocale        string  `json:"preferred_locale"`
		NitroBoosters          uint64  `json:"premium_subscription_count,omitempty"`
		MaxMembers             uint64  `json:"max_members,omitempty"`
		MaxPresences           uint64  `json:"max_presences,omitempty"`
		RulesChannelID         string  `json:"rules_channel_id"`
		AppID                  string  `json:"application_id,omitempty"`
		Emojis                 []Emoji `json:"emojis"`
		Roles                  []Role  `json:"roles"`
		AFKTimeout             string  `json:"afk_timeout"`
		AFKChannelID           string  `json:"afk_channel_id,omitempty"`
		VoiceRegion            string  `json:"region"`
		WidgetChannelID        string  `json:"widget_channel_id,omitempty"`
		Permissions            string  `json:"permissions,string,omitempty"`
		SystemChannelID        string  `json:"system_channel_id,omitempty"`
		Features []GuildFeature `json:"guild_features"`
	*/
}
