package ndarchive

type Timestamp string

type Message struct {
	ID      Snowflake `json:"id"`
	Content string    `json:"content,omitempty"`

	Author uint `json:"author"`

	Type  uint `json:"type,omitempty"`
	Flags uint `json:"flags,omitempty"`

	Timestamp       Timestamp `json:"timestamp"`
	EditedTimestamp Timestamp `json:"edited_timestamp,omitempty"`

	TTS    bool `json:"tts,omitempty"`
	Pinned bool `json:"pinned,omitempty"`

	MentionEveryone bool        `json:"mention_everyone,omitempty"`
	MentionUser     []uint      `json:"mentions,omitempty"`
	MentionRoles    []Snowflake `json:"mention_roles,omitempty"`
	MentionChannels []Snowflake `json:"mention_channels,omitempty"`

	Attachments []Attachment `json:"attachments,omitempty"`
	Embeds      []Embed      `json:"embeds,omitempty"`
	Reactions   []Reaction   `json:"reactions,omitempty"`

	Reference *MessageReference `json:"message_reference,omitempty"`

	Stickers []Sticker `json:"stickers,omitempty"`

	// --- potentially removable fields
	// Components        []ComponentWrap     `json:"components,omitempty"`
	// Activity    *MessageActivity    `json:"activity,omitempty"`
	// Application *MessageApplication `json:"application,omitempty"`
	// ReferencedMessage *Message            `json:"referenced_message,omitempty"`

	// --- deliberately omited fields
	/*
		ChannelID string `json:"channel_id"`           // already obvious by the filename
		GuildID   string `json:"guild_id,omitempty"`   // doesn't really serve a purpose here as there are no cross guild archives, even if multiple guilds are included in future versions we can just put that bit in the channel info
		WebhookID string `json:"webhook_id,omitempty"` // isn't this redundant with author
	*/

	// if a bot has custom information it wants to attatch, it can do so here
	Custom CustomValues `json:"custom,omitempty"`
}

type Reaction struct {
	Count int `json:"count,omitempty"`
	//	Me    bool  `json:"me"`
	Emoji Emoji `json:"emoji,omitempty"`
}

type Attachment struct {
	ID          Snowflake `json:"id"`
	Filename    string    `json:"filename"`
	URL         string    `json:"url"`
	ContentType string    `json:"content_type,omitempty"`

	// potentially not needed fields?
	//Size   uint64 `json:"size,omitempty"`
	//Proxy  string `json:"proxy_url,omitempty"`
	//Height uint   `json:"height,omitempty"`
	//Width  uint   `json:"width,omitempty"`
}

type MessageReference struct {
	MessageID Snowflake `json:"message_id,omitempty"`
	ChannelID Snowflake `json:"channel_id,omitempty"`
	GuildID   Snowflake `json:"guild_id,omitempty"`
}

type Sticker struct {
	ID         Snowflake `json:"id"`
	PackID     Snowflake `json:"pack_id,omitempty"`
	Name       string    `json:"name"`
	Type       uint      `json:"type"`
	FormatType uint      `json:"format_type"`

	// probably not needed
	/*
		Description string `json:"description"`
		Tags        string `json:"tags"`
		GuildID     string `json:"guild_id,omitempty"`
		Available   bool   `json:"available,omitempty"`
		User        *User  `json:"user,omitempty"`
		SortValue   *int   `json:"sort_value,omitempty"`
	*/
}

// maybe not needed
//type MessageActivity struct {
//	Type    uint   `json:"type"`
//	PartyID string `json:"party_id,omitempty"`
//}

// maybe not needed
//type MessageApplication struct {
//	ID          string `json:"id,omitempty"`
//	CoverID     string `json:"cover_image,omitempty"`
//	Description string `json:"description,omitempty"`
//	Icon        string `json:"icon,omitempty"`
//	Name        string `json:"name,omitempty"`
//}
