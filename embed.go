package ndarchive

// Embed is the data for all discord embed fields
type Embed struct {
	Title       string          `json:"title,omitempty"`
	Description string          `json:"description,omitempty"`
	URL         string          `json:"url,omitempty"`
	Timestamp   Timestamp       `json:"timestamp,omitempty"` // an ISO8601 or RFC3339 timestamp to mark the embed with
	Color       uint            `json:"color,omitempty"`     // A color in RGB integer format (basically an RGB hex code converted to decimal)
	Footer      *EmbedFooter    `json:"footer,omitempty"`
	Image       *EmbedImage     `json:"image,omitempty"`
	Thumbnail   *EmbedThumbnail `json:"thumbnail,omitempty"`
	Video       *EmbedVideo     `json:"video,omitempty"`
	Author      *EmbedAuthor    `json:"author,omitempty"`
	Fields      []EmbedField    `json:"fields,omitempty"`
}

// EmbedFooter contains data for a discord embed footer field
type EmbedFooter struct {
	Text    string `json:"text,omitempty"`
	IconURL string `json:"icon_url,omitempty"`
}

// EmbedImage contains data for a discord embed image field
type EmbedImage struct {
	URL string `json:"url,omitempty"`
	// Height int    `json:"height,omitempty"` // necessary?
	// Width  int    `json:"width,omitempty"`
}

// EmbedThumbnail contains data for a discord embed thumbnail field
type EmbedThumbnail struct {
	URL string `json:"url,omitempty"`
	// Height int    `json:"height,omitempty"`
	// Width  int    `json:"width,omitempty"`
}

// EmbedVideo contains data for a discord embed video field
type EmbedVideo struct {
	URL string `json:"url,omitempty"`
	// Height int    `json:"height,omitempty"`
	// Width  int    `json:"width,omitempty"`
}

// EmbedAuthor contains data for the discord embed author field
type EmbedAuthor struct {
	Name    string `json:"name,omitempty"`
	URL     string `json:"url,omitempty"`
	IconURL string `json:"icon_url,omitempty"`
}

// EmbedField contains data for a discord embed field
type EmbedField struct {
	Name   string `json:"name,omitempty"`
	Value  string `json:"value,omitempty"`
	Inline bool   `json:"inline,omitempty"`
}
